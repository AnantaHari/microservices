https://plantuml.com/guide

https://www.plantuml.com/plantuml/png/ZO-nQiKm34LtVuN8MZ9qpVI6jnGUNK8-G8b58s8xHP9BG-c_rosbY9H25wDpVBcsBceeLdCAIq9Lc9G4tWBSSHnWcXwkRIoOQO3HXCjJGwroCfbZJFHuYj7h03TMQ_FCOcl4Ou0h6ZMni1qkYozeA3SkcvVpNirnuMKxFRoYn77ZbDH9wZDoSk9vtGlzy7luNdMiiox2i_5UpYjN9MdGpYzK5_sxY4J_sUDaQmcV8tJt7Unz3rtfVEFlI__lSA4IQquV

@startuml
class User {
 +id: UUID
 +name: String
 +surname: String
 +middleName: String
 +sex: List
 +birthday: Date
 +city: String
 +avatarLink: String
 +about: String
 +nickname: String
 +hardSkills: String
 +e-mail: String
 +phone: String
}

class Subscribtion {
 +id: UUID
 +user: UserID
 +subscribtion: String
}

class Subscribers {
 +id: UUID
 +user: UserID
 +subscriber: UserID
}


User "1" o-- "n" Subscribers
User "1" o-- "n" Subscribtion
@enduml
